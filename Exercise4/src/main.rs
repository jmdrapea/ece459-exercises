use std::thread;
use std::time::Duration;
use std::sync::mpsc;

fn main() {
    let (tx, rx) = mpsc::channel();

	// if we were going to spawn more than one thread, we would
	// have to clone tx and reference the cloned copy inside the
	// thread
    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];
        
        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}

// use std::sync::mpsc;
// use std::thread;
// use std::time::Duration;

// static N: i32 = 10; // # of threads
// static M: i32 = 3; // # of send per thread
// // Print NxM lines of received String

// // You should modify main() to spawn threads and communicate using channels
// fn main() {
//     let mut children = vec![];
//     let (tx, rx) = mpsc::channel();

//     for i in 0..N {
//         let tx1 = mpsc::Sender::clone(&tx);
//         // or thread_tx = tx.clone();
//         children.push(thread::spawn(move || {
//             for _i in 0..M {
//                 tx1.send(String::from("Hi from thread ".to_owned() + &i.to_string())).unwrap();
//                 thread::sleep(Duration::from_secs(1));
//             }
//         }));
//     }

//     //rx.recv().unwrap();
//     for (i, received) in rx.iter().enumerate() {
//         println!("Got #{}: {}", i+1, received);
//     }
// }

//https://doc.rust-lang.org/rust-by-example/std_misc/channels.html
// use std::sync::mpsc::{Sender, Receiver};
// use std::sync::mpsc;
// use std::thread;

// static NTHREADS: i32 = 3;

// fn main() {
//     // Channels have two endpoints: the `Sender<T>` and the `Receiver<T>`,
//     // where `T` is the type of the message to be transferred
//     // (type annotation is superfluous)
//     let (tx, rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();
//     let mut children = Vec::new();

//     for id in 0..NTHREADS {
//         // The sender endpoint can be copied
//         let thread_tx = tx.clone();

//         // Each thread will send its id via the channel
//         let child = thread::spawn(move || {
//             // The thread takes ownership over `thread_tx`
//             // Each thread queues a message in the channel
//             thread_tx.send(id).unwrap();

//             // Sending is a non-blocking operation, the thread will continue
//             // immediately after sending its message
//             println!("thread {} finished", id);
//         });

//         children.push(child);
//     }

//     // Here, all the messages are collected
//     let mut ids = Vec::with_capacity(NTHREADS as usize);
//     for _ in 0..NTHREADS {
//         // The `recv` method picks a message from the channel
//         // `recv` will block the current thread if there are no messages available
//         ids.push(rx.recv());
//     }
    
//     // Wait for the threads to complete any remaining work
//     for child in children {
//         child.join().expect("oops! the child thread panicked");
//     }

//     // Show the order in which the messages were sent
//     println!("{:?}", ids);
// }
