// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    match n {
        0 => 0,
        1 => 1,
        _ => fibonacci_number(n-1) + fibonacci_number(n-2),
    }
}
// use expect() for error messages, unwrap() can be done, or match

fn fibonacci_number_non_recursive(n: u32) -> u32 {
	let mut a = 0;
	let mut b = 1;
	let mut c = 1;
	for _ in 1..n {
		c = a + b;
		a = b;
		b = c;
	}
	c
}



fn main() {
    println!("{}", fibonacci_number(10));
    println!("{}", fibonacci_number_non_recursive(10));
}
